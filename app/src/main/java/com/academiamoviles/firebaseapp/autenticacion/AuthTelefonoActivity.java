package com.academiamoviles.firebaseapp.autenticacion;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import com.academiamoviles.firebaseapp.R;
import com.google.firebase.FirebaseException;
import com.google.firebase.FirebaseTooManyRequestsException;
import com.google.firebase.auth.FirebaseAuthInvalidCredentialsException;
import com.google.firebase.auth.PhoneAuthCredential;
import com.google.firebase.auth.PhoneAuthProvider;

import java.util.concurrent.TimeUnit;

public class AuthTelefonoActivity extends AppCompatActivity {

    Button btnEnviarCodigo;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_auth_telefono);

        btnEnviarCodigo = findViewById(R.id.btnEnviarCodigo);

        btnEnviarCodigo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                try {

                    final String telefono = "+" + "51" + "960622439";

                    PhoneAuthProvider.getInstance().verifyPhoneNumber(
                            telefono,
                            60,
                            TimeUnit.SECONDS,
                            AuthTelefonoActivity.this,
                            new PhoneAuthProvider.OnVerificationStateChangedCallbacks() {
                                @Override
                                public void onVerificationCompleted(PhoneAuthCredential phoneAuthCredential) {
                                    Log.i("jledesma","onVerificationCompleted");
                                }

                                @Override
                                public void onVerificationFailed(FirebaseException e) {

                                    Log.i("jledesma","onVerificationFailed");

                                    if (e instanceof FirebaseAuthInvalidCredentialsException) {

                                        Toast.makeText(AuthTelefonoActivity.this,
                                        "Teléfono inválido. Verifica que el código del país y el número sean correctos",
                                                Toast.LENGTH_SHORT).show();

                                        return;
                                    } else if (e instanceof FirebaseTooManyRequestsException) {

                                        Toast.makeText(AuthTelefonoActivity.this,
                                        "Ha sobrepasado el limite de envios diarios",
                                                Toast.LENGTH_SHORT).show();
                                        return;
                                    }
                                }

                                @Override
                                public void onCodeSent(String s, PhoneAuthProvider.ForceResendingToken forceResendingToken) {
                                    super.onCodeSent(s, forceResendingToken);

                                    Log.i("jledesma","onCodeSent");


                                    Bundle bundle = new Bundle();
                                    bundle.putString("AuthCredentials", s);
                                    bundle.putString("PhoneNumber", telefono);

                                    Intent intent = new Intent(AuthTelefonoActivity.this, RecibirCodigoActivity.class);
                                    intent.putExtras(bundle);
                                    startActivity(intent);
                                }
                            });
                } catch (Exception e) {
                    Toast.makeText(AuthTelefonoActivity.this,e.getMessage(), Toast.LENGTH_SHORT).show();
                }

            }
        });
    }
}