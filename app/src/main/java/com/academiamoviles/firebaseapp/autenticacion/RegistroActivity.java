package com.academiamoviles.firebaseapp.autenticacion;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.academiamoviles.firebaseapp.AuthActivity;
import com.academiamoviles.firebaseapp.R;
import com.academiamoviles.firebaseapp.modelo.Usuario;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.firestore.CollectionReference;
import com.google.firebase.firestore.FirebaseFirestore;

public class RegistroActivity extends AppCompatActivity {

    private FirebaseAuth fAuth = FirebaseAuth.getInstance();
    private FirebaseFirestore db = FirebaseFirestore.getInstance();
    private CollectionReference collectionUsuario = db.collection("Usuario");

    EditText edtNombres,edtApelliidos,edtCorreo,edtClaveRegistro;
    Button btnRegistrar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_registro);

        edtNombres = findViewById(R.id.edtNombres);
        edtApelliidos = findViewById(R.id.edtApelliidos);
        edtCorreo = findViewById(R.id.edtCorreo);
        edtClaveRegistro = findViewById(R.id.edtClaveRegistro);
        btnRegistrar = findViewById(R.id.btnRegistrar);

        if (fAuth.getCurrentUser() != null){
            Intent intent = new Intent(this,MenuActivity.class);
            startActivity(intent);
        }


        btnRegistrar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                registrarUsuario();
            }
        });
    }

    private void registrarUsuario(){

        String correo = edtCorreo.getText().toString();
        String clave = edtClaveRegistro.getText().toString();

        fAuth.createUserWithEmailAndPassword(correo,clave).addOnCompleteListener(new OnCompleteListener<AuthResult>() {
            @Override
            public void onComplete(@NonNull Task<AuthResult> task) {

                if (task.isSuccessful()) {

                    String idUsuario = fAuth.getCurrentUser().getUid();

                    Usuario usuario = new Usuario(idUsuario, edtNombres.getText().toString(),
                            edtApelliidos.getText().toString(), edtCorreo.getText().toString(), edtClaveRegistro.getText().toString());

                    collectionUsuario.document(idUsuario).set(usuario)
                            .addOnCompleteListener(new OnCompleteListener<Void>() {
                                @Override
                                public void onComplete(@NonNull Task<Void> task) {
                                    Toast.makeText(RegistroActivity.this, "Usuario creado", Toast.LENGTH_SHORT).show();

                                    Intent intent = new Intent(RegistroActivity.this, AuthActivity.class);
                                    startActivity(intent);
                                }
                            }).addOnFailureListener(new OnFailureListener() {
                        @Override
                        public void onFailure(@NonNull Exception e) {
                            Toast.makeText(RegistroActivity.this, e.getMessage(), Toast.LENGTH_SHORT).show();
                        }
                    });
                }
            }
        });
    }
}
