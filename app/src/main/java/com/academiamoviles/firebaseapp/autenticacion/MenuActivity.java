package com.academiamoviles.firebaseapp.autenticacion;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import com.academiamoviles.firebaseapp.AuthActivity;
import com.academiamoviles.firebaseapp.R;
import com.google.firebase.auth.FirebaseAuth;

public class MenuActivity extends AppCompatActivity {

    FirebaseAuth fAuth = FirebaseAuth.getInstance();

    Button btnInicio;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_menu);

        btnInicio = findViewById(R.id.btnInicio);

        btnInicio.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                fAuth.signOut();

                Intent intent = new Intent(MenuActivity.this, AuthActivity.class);
                startActivity(intent);
            }
        });
    }
}