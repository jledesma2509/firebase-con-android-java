package com.academiamoviles.firebaseapp.autenticacion;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;

import com.firebase.ui.auth.AuthUI;
import com.firebase.ui.auth.IdpResponse;
import com.academiamoviles.firebaseapp.R;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;

import java.util.Arrays;
import java.util.List;

public class GmailActivity extends AppCompatActivity {

    Button btnMetodo;

    FirebaseAuth fAuth = FirebaseAuth.getInstance();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_gmail);

        if (fAuth.getCurrentUser() != null) {
            FirebaseUser user = fAuth.getCurrentUser();

            Log.i("jledesma",user.getEmail());
            Log.i("jledesma",user.getUid());
            Log.i("jledesma",user.getDisplayName());
            Log.i("jledesma",user.getPhotoUrl().toString());

            Intent intent = new Intent(this, MenuGmailActivity.class);
            startActivity(intent);
            this.finish();
        }

        btnMetodo = findViewById(R.id.btnMetodo);

        btnMetodo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                auth();
            }
        });


    }

    private void auth() {

        List<AuthUI.IdpConfig> providers = Arrays.asList(
                //new AuthUI.IdpConfig.EmailBuilder().build(),
                new AuthUI.IdpConfig.GoogleBuilder().build()
                //new AuthUI.IdpConfig.PhoneBuilder().build()
        );



        Intent intent = AuthUI.getInstance()
                .createSignInIntentBuilder()
                .setAvailableProviders(providers)
                .setTosAndPrivacyPolicyUrls("https://example.com", "https://example.com")
                .setLogo(R.drawable.charizard)
                .setAlwaysShowSignInMethodScreen(true)
                .setIsSmartLockEnabled(false)
                .build();

        startActivityForResult(intent, 1000);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == 1000) {
            if (resultCode == RESULT_OK) {
                // We have signed in the user or we have a new user
                FirebaseUser user = FirebaseAuth.getInstance().getCurrentUser();

                Log.i("jledesma",user.getEmail());
                Log.i("jledesma",user.getUid());
                Log.i("jledesma",user.getDisplayName());
                Log.i("jledesma",user.getPhotoUrl().toString());

                Intent intent = new Intent(this, MenuGmailActivity.class);
                startActivity(intent);
                this.finish();

            } else {
                // Signing in failed
                IdpResponse response = IdpResponse.fromResultIntent(data);
                if (response == null) {
                    Log.d("TAG", "onActivityResult: the user has cancelled the sign in request");
                } else {
                    Log.e("TAG", "onActivityResult: ", response.getError());
                }
            }
        }
    }


}