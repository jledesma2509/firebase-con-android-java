package com.academiamoviles.firebaseapp.autenticacion;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import com.academiamoviles.firebaseapp.R;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.PhoneAuthCredential;
import com.google.firebase.auth.PhoneAuthProvider;

import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class RecibirCodigoActivity extends AppCompatActivity {

    FirebaseAuth mFirebaseAuth = FirebaseAuth.getInstance();

    Button btnValidarCodigo;
    EditText edtCodigoTelefono;

    String telefono, credenciales;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_recibir_codigo);

        Log.i("jledesma","Init");

        btnValidarCodigo = findViewById(R.id.btnValidarCodigo);
        edtCodigoTelefono = findViewById(R.id.edtCodigoTelefono);


        Bundle bundle = getIntent().getExtras();
        telefono = bundle.getString("PhoneNumber");
        credenciales = bundle.getString("AuthCredentials");


        btnValidarCodigo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                String codigoValidacion = edtCodigoTelefono.getText().toString();
                verificarCodigo(codigoValidacion,telefono);
            }
        });
    }

    private void verificarCodigo(String codigoValidacion, String telefono) {

        PhoneAuthCredential phoneAuthCredential = PhoneAuthProvider.getCredential(credenciales, codigoValidacion);
        signInWithPhoneAuthCredential(phoneAuthCredential);
    }

    private void signInWithPhoneAuthCredential(PhoneAuthCredential phoneAuthCredential) {

        try {
            mFirebaseAuth.signInWithCredential(phoneAuthCredential).addOnCompleteListener(new OnCompleteListener<AuthResult>() {
                @Override
                public void onComplete(@NonNull Task<AuthResult> task) {

                    Log.i("jledesma","onComplete");

                    if (task.isSuccessful()) {

                        Toast.makeText(RecibirCodigoActivity.this, "Validado", Toast.LENGTH_SHORT).show();

                    } else {

                        Toast.makeText(RecibirCodigoActivity.this, task.getException().toString(), Toast.LENGTH_SHORT).show();

                    }
                }
            }).addOnFailureListener(new OnFailureListener() {
                @Override
                public void onFailure(@NonNull Exception e) {

                    Toast.makeText(RecibirCodigoActivity.this, e.getMessage(), Toast.LENGTH_SHORT).show();
                }
            });
        }catch (Exception ex){
            Toast.makeText(RecibirCodigoActivity.this, ex.getMessage(), Toast.LENGTH_SHORT).show();
        }


    }
}