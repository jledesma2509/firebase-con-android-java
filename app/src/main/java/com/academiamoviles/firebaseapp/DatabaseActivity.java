package com.academiamoviles.firebaseapp;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.academiamoviles.firebaseapp.modelo.Nota;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.firestore.CollectionReference;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.EventListener;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.FirebaseFirestoreException;
import com.google.firebase.firestore.ListenerRegistration;
import com.google.firebase.firestore.QuerySnapshot;

import java.util.HashMap;
import java.util.Map;

import javax.annotation.Nullable;

public class DatabaseActivity extends AppCompatActivity {

    private static final String TITULO = "TITULO";
    private static final String DESCRIPCION = "DESCRIPCION";

    Button btnGrabar,btnObtener,btnActualizar,btnEliminar;
    EditText edtTitulo,edtDescripcion;

    ListenerRegistration listen;

    private FirebaseFirestore db  = FirebaseFirestore.getInstance();
    private CollectionReference collectionNotas = db.collection("Notas");

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_database);

        btnGrabar = findViewById(R.id.btnGrabar);
        btnObtener = findViewById(R.id.btnObtener);
        btnActualizar = findViewById(R.id.btnActualizar);
        btnEliminar = findViewById(R.id.btnEliminar);

        edtTitulo = findViewById(R.id.edtTitulo);
        edtDescripcion = findViewById(R.id.edtDescripcion);

        btnGrabar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                grabarNota();
            }
        });

        btnObtener.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                obtenerInformacion();
            }
        });

        btnActualizar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                actualizarNota();
            }
        });

        btnEliminar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                eliminarNota();
            }
        });

    }

    @Override
    protected void onDestroy() {
        super.onDestroy();

        listen.remove();

    }

    @Override
    protected void onStart() {
        super.onStart();

        //Filtrar uno en especifico
        /*listen = collectionNotas.document("5RFaxF8zbfdcESlyjEhl")
                .addSnapshotListener(this, new EventListener<DocumentSnapshot>() {
                    @Override
                    public void onEvent(@Nullable DocumentSnapshot documentSnapshot, @Nullable FirebaseFirestoreException e) {
                        if (documentSnapshot.exists()) {

                            Nota nota = documentSnapshot.toObject(Nota.class);
                            String id = documentSnapshot.getId();
                            //String titulo = document.getData() getString(TITULO);
                            //String descripcion = document.getString(DESCRIPCION).toString();

                            Log.i("jledesma", id);
                            Log.i("jledesma", nota.getTITULO());
                            Log.i("jledesma", nota.getDESCRIPCION());
                            Log.i("jledesma", "------------");
                        }
                    }
                });*/

        //Filtrar todos
        listen = collectionNotas.addSnapshotListener(this, new EventListener<QuerySnapshot>() {
            @Override
            public void onEvent(@Nullable QuerySnapshot queryDocumentSnapshots, @Nullable FirebaseFirestoreException e) {

                for (DocumentSnapshot document : queryDocumentSnapshots){

                    Nota nota = document.toObject(Nota.class);
                    String id = document.getId();

                    Log.i("jledesma",id);
                    Log.i("jledesma",nota.getTITULO());
                    Log.i("jledesma",nota.getDESCRIPCION());
                    Log.i("jledesma","------------");

                }
            }
        });
    }

    private void obtenerInformacion(){

          //Buscar por un documento en especifico
          /*collectionNotas.document("5RFaxF8zbfdcESlyjEhl").get().
                  addOnCompleteListener(new OnCompleteListener<DocumentSnapshot>() {
              @Override
              public void onComplete(@NonNull Task<DocumentSnapshot> task) {

                  DocumentSnapshot document = task.getResult();

                  Nota nota = document.toObject(Nota.class);
                  String id = document.getId();
                  //String titulo = document.getData() getString(TITULO);
                  //String descripcion = document.getString(DESCRIPCION).toString();

                  Log.i("jledesma", id);
                  Log.i("jledesma", nota.getTITULO());
                  Log.i("jledesma", nota.getDESCRIPCION());
                  Log.i("jledesma", "------------");
              }
          });

          //Buscar todos
        //Sin filtro
        collectionNotas.get().addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {
            @Override
            public void onComplete(@NonNull Task<QuerySnapshot> task) {

                for (DocumentSnapshot document : task.getResult()){

                    Nota nota = document.toObject(Nota.class);
                    String id = document.getId();

                    Log.i("jledesma",id);
                    Log.i("jledesma",nota.getTITULO());
                    Log.i("jledesma",nota.getDESCRIPCION());
                    Log.i("jledesma","------------");

                }
            }
        });*/

        //Filtro por campo
        collectionNotas.whereEqualTo(TITULO,"Android").get().addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {
            @Override
            public void onComplete(@NonNull Task<QuerySnapshot> task) {

                for (DocumentSnapshot document : task.getResult()){

                    Nota nota = document.toObject(Nota.class);
                    String id = document.getId();

                    Log.i("jledesma",id);
                    Log.i("jledesma",nota.getTITULO());
                    Log.i("jledesma",nota.getDESCRIPCION());
                    Log.i("jledesma","------------");

                }
            }
        });

    }

    private void eliminarNota(){

        String documento = "MiPrimeraNota";

        db.collection("Notas").document(documento).delete().
                addOnCompleteListener(new OnCompleteListener<Void>() {
                    @Override
                    public void onComplete(@NonNull Task<Void> task) {
                        Toast.makeText(DatabaseActivity.this, "Nota eliminada", Toast.LENGTH_SHORT).show();
                    }
                }).addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception e) {
                Toast.makeText(DatabaseActivity.this, e.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });
    }

    private void actualizarNota(){

        String titulo = edtTitulo.getText().toString();
        String descripcion = edtDescripcion.getText().toString();

        Map<String, Object> nota = new HashMap<>();
        nota.put(TITULO, titulo);
        nota.put(DESCRIPCION, descripcion);

        db.collection("Notas").
                document("5RFaxF8zbfdcESlyjEhl").update(nota).
                addOnCompleteListener(new OnCompleteListener<Void>() {
                    @Override
                    public void onComplete(@NonNull Task<Void> task) {
                        Toast.makeText(DatabaseActivity.this, "Nota actualizada", Toast.LENGTH_SHORT).show();
                    }
                }).addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception e) {
                Toast.makeText(DatabaseActivity.this, e.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });
    }

    private void grabarNota() {

        String titulo = edtTitulo.getText().toString();
        String descripcion = edtDescripcion.getText().toString();

        Map<String, Object> nota = new HashMap<>();
        nota.put(TITULO, titulo);
        nota.put(DESCRIPCION, descripcion);

        db.collection("Notas").add(nota).addOnCompleteListener(new OnCompleteListener<DocumentReference>() {
            @Override
            public void onComplete(@NonNull Task<DocumentReference> task) {
                Toast.makeText(DatabaseActivity.this, "Nota creada", Toast.LENGTH_SHORT).show();
            }
        }).addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception e) {
                Toast.makeText(DatabaseActivity.this, e.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });

        /*
        db.collection("Notas").
            document("MiPrimeraNota").set(nota).addOnCompleteListener(new OnCompleteListener<Void>() {
            @Override
            public void onComplete(@NonNull Task<Void> task) {
                Toast.makeText(DatabaseActivity.this, "Nota creada", Toast.LENGTH_SHORT).show();
            }
        }).addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception e) {
                Toast.makeText(DatabaseActivity.this, e.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });
         */

    }
}