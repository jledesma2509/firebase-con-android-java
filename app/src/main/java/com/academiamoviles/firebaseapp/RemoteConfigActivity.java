package com.academiamoviles.firebaseapp;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.net.Uri;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.remoteconfig.FirebaseRemoteConfig;
import com.google.firebase.remoteconfig.FirebaseRemoteConfigSettings;

import java.util.HashMap;
import java.util.Map;

public class RemoteConfigActivity extends AppCompatActivity {

    FirebaseRemoteConfig firebaseRemoteConfig;

    Button btnObtenerValores,btnVerificarVersion;
    TextView tvMensaje;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_remote_config);

        btnObtenerValores = findViewById(R.id.btnObtenerValores);
        btnVerificarVersion =  findViewById(R.id.btnVerificarVersion);
        tvMensaje = findViewById(R.id.tvMensaje);

        firebaseRemoteConfig = FirebaseRemoteConfig.getInstance();
        FirebaseRemoteConfigSettings configSettings = new FirebaseRemoteConfigSettings.Builder().build();
        firebaseRemoteConfig.setConfigSettings(configSettings);

        /*Map<String, Object> config = new HashMap<>();
        config.put("color", "#D81B60");
        config.put("size", "14");
        firebaseRemoteConfig.setDefaults(config);*/

        final long cacheExpiration = 3600; // 1 hour in seconds.

        btnObtenerValores.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                firebaseRemoteConfig.fetch(cacheExpiration).addOnCompleteListener(new OnCompleteListener<Void>() {
                    @Override
                    public void onComplete(@NonNull Task<Void> task) {
                        if (task.isSuccessful()) {
                            firebaseRemoteConfig.activateFetched();
                            String fontSize = firebaseRemoteConfig.getString("size");
                            String fontColor = firebaseRemoteConfig.getString("color");
                            tvMensaje.setTextSize(Float.parseFloat(fontSize));
                            tvMensaje.setTextColor(Color.parseColor(fontColor));
                        }
                        else{
                            Toast.makeText(RemoteConfigActivity.this, task.getException().toString(), Toast.LENGTH_SHORT).show();
                        }
                    }
                }).addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception e) {
                        Toast.makeText(RemoteConfigActivity.this, e.getMessage(), Toast.LENGTH_SHORT).show();
                    }
                });
            }
        });

        btnVerificarVersion.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                firebaseRemoteConfig.fetch(cacheExpiration).addOnCompleteListener(new OnCompleteListener<Void>() {
                    @Override
                    public void onComplete(@NonNull Task<Void> task) {
                        if (task.isSuccessful()) {
                            firebaseRemoteConfig.activateFetched();
                            String version_actualizada = firebaseRemoteConfig.getString("version");

                            try {
                                PackageInfo pInfo = RemoteConfigActivity.this.getPackageManager().getPackageInfo(getPackageName(), 0);
                                int version_actual = pInfo.versionCode;

                                if (version_actual < Integer.parseInt(version_actualizada)){
                                    //Mostrar dialogo
                                    mostrarDialogo().show();
                                }
                                else{
                                    Toast.makeText(RemoteConfigActivity.this, "La aplicacion esta actualizada", Toast.LENGTH_SHORT).show();
                                }

                            } catch (PackageManager.NameNotFoundException e) {
                                e.printStackTrace();
                            }
                        }
                        else{
                            Toast.makeText(RemoteConfigActivity.this, task.getException().toString(), Toast.LENGTH_SHORT).show();
                        }
                    }
                }).addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception e) {
                        Toast.makeText(RemoteConfigActivity.this, e.getMessage(), Toast.LENGTH_SHORT).show();
                    }
                });
            }
        });
    }

    public AlertDialog mostrarDialogo() {

        final AlertDialog alertDialog;
        final AlertDialog.Builder builder = new AlertDialog.Builder(this);

        LayoutInflater inflater = getLayoutInflater();

        View v = inflater.inflate(R.layout.dialogo, null);

        builder.setView(v);

        Button btn_aperturar_no =  v.findViewById(R.id.btn_version_no);
        Button btn_aperturar_si =  v.findViewById(R.id.btn_version_si);

        alertDialog = builder.create();


        btn_aperturar_si.setOnClickListener(
                new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {

                       String url = "https://play.google.com/store/apps/details?id=com.ubercab";
                       Intent i = new Intent(Intent.ACTION_VIEW);
                       i.setData(Uri.parse(url));
                       startActivity(i);
                    }
                }
        );

        btn_aperturar_no.setOnClickListener(
                new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {

                        alertDialog.dismiss();

                    }
                }

        );

        return alertDialog;
    }
}