package com.academiamoviles.firebaseapp;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import com.crashlytics.android.Crashlytics;

public class CrashlyticsActivity extends AppCompatActivity {

    Button btnTestException,btnNullPointer;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_crashlytics);

        btnTestException = findViewById(R.id.btnTestException);
        btnNullPointer = findViewById(R.id.btnNullPointer);

        btnTestException.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Crashlytics.getInstance().crash();
            }
        });

        btnNullPointer.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                throw new NullPointerException();
            }
        });

    }
}