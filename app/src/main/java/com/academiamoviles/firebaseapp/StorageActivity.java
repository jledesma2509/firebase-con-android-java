package com.academiamoviles.firebaseapp;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import android.content.ContentResolver;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.View;
import android.webkit.MimeTypeMap;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.academiamoviles.firebaseapp.modelo.Nota;
import com.academiamoviles.firebaseapp.modelo.Upload;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.firestore.CollectionReference;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.QuerySnapshot;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.OnProgressListener;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

public class StorageActivity extends AppCompatActivity {

    private static final int PICK_IMAGE = 1;

    private Uri imageUri;

    private FirebaseFirestore db = FirebaseFirestore.getInstance();
    private CollectionReference collectionImagenes = db.collection("Imagenes");

    private StorageReference storageReference = FirebaseStorage.getInstance().getReference("upload");


    private List<Upload> listImagenes = new ArrayList<Upload>();

    Button choose, upload, showLog;
    ImageView imgImagen;
    EditText edtNombreArchivo;
    ProgressBar progress;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_storage);

        choose = findViewById(R.id.choose);
        upload = findViewById(R.id.upload);
        showLog = findViewById(R.id.showLog);
        imgImagen = findViewById(R.id.imgImagen);
        edtNombreArchivo = findViewById(R.id.edtNombreArchivo);
        progress = findViewById(R.id.progress);

        choose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                escogerImagen();
            }
        });

        upload.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                subirImagen();
            }
        });

        showLog.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mostrarImagen();
            }
        });
    }

    private void mostrarImagen() {

        //Sin filtro
        collectionImagenes.get().addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {
            @Override
            public void onComplete(@NonNull Task<QuerySnapshot> task) {

                for (DocumentSnapshot document : task.getResult()){

                    Upload upload = document.toObject(Upload.class);
                    String id = document.getId();

                    Log.i("jledesma",id);
                    Log.i("jledesma",upload.getName());
                    Log.i("jledesma",upload.getImage());
                    Log.i("jledesma","------------");

                }
            }
        });
    }

    private void escogerImagen() {

        Intent intent = new Intent();
        intent.setType("image/*");
        intent.setAction(Intent.ACTION_GET_CONTENT);
        startActivityForResult(intent, PICK_IMAGE);
    }

    private String obtenerExtension(Uri uri) {

        ContentResolver contentResolver = getContentResolver();
        MimeTypeMap mine = MimeTypeMap.getSingleton();
        return mine.getExtensionFromMimeType(contentResolver.getType(uri));

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);


        if (requestCode == PICK_IMAGE && resultCode == RESULT_OK) {

            imageUri = data.getData();
            Picasso.get().load(imageUri).into(imgImagen);

        }
    }

    public void subirImagen() {

        if (imageUri != null) {

            final StorageReference storageReference2 =
                    storageReference.child(edtNombreArchivo.getText().toString() + "." +
                            obtenerExtension(imageUri));

            storageReference2.putFile(imageUri).addOnCompleteListener(new OnCompleteListener<UploadTask.TaskSnapshot>() {
                @Override
                public void onComplete(@NonNull Task<UploadTask.TaskSnapshot> task) {

                    Handler handler = new Handler();

                    handler.postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            progress.setProgress(0);

                            Toast.makeText(StorageActivity.this, "Subido correctamente", Toast.LENGTH_SHORT).show();

                            storageReference2.getDownloadUrl().addOnCompleteListener(new OnCompleteListener<Uri>() {
                                @Override
                                public void onComplete(@NonNull Task<Uri> task) {
                                    Upload upload = new Upload(edtNombreArchivo.getText().toString(), task.getResult().toString());
                                    collectionImagenes.add(upload);
                                }
                            });
                        }
                    }, 5000);
                }
            }).addOnFailureListener(new OnFailureListener() {
                @Override
                public void onFailure(@NonNull Exception e) {
                    Toast.makeText(StorageActivity.this, e.getMessage(), Toast.LENGTH_SHORT).show();
                }
            }).addOnProgressListener(new OnProgressListener<UploadTask.TaskSnapshot>() {
                @Override
                public void onProgress(UploadTask.TaskSnapshot taskSnapshot) {
                    final Double progressInfo = (100.0 * taskSnapshot.getBytesTransferred() / taskSnapshot.getTotalByteCount());
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            progress.setProgress(progressInfo.intValue());
                        }
                    });
                }
            });

        } else {
            //Mensaje
        }
    }

}